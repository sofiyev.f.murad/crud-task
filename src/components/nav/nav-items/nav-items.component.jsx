import React from "react";

import NavItem from "../nav-item/nav-item.component";

function NavItems() {
    return (
        <ul className="nav-items">
            <NavItem />
        </ul>
    )
}

export default NavItems;