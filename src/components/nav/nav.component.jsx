import React from "react";
import classNames from "classnames";

import NavItems from "./nav-items/nav-items.component";

import "./nav.style.scss"


function Nav({className}) {
    const classes = classNames("nav", className, {

    })
    return (
        <nav className={classes}>
            <NavItems />
        </nav>
    )
}

export default Nav;