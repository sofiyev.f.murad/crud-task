import React from "react";
import { Link } from "react-router-dom"

import "./nav-item.style.scss"

function NavItem() {
    return (
        <li className="nav-item">
            <Link className="nav-item__link" to="/"> Posts </Link>
        </li>
    )
}

export default NavItem;