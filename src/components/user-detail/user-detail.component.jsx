import React from "react";

import "./user-detail.style.scss";

function UserDetail({ userData, address }) {

    const { name, username, email, phone, website, company } = userData;
    const { city } = address;

    return (
        <div className="user-detail">
            <div className="user-detail__header">
                <h2 className="user-detail__name">{name}</h2>
                <span className="user-detail__city">{city}</span>
            </div>

            <div className="user-detail__content">
                <ul className="user-detail__info-list">
                    <li className="user-detail__info-item">
                        <span className="user-detail__label">Username</span>
                        <span className="user-detail__text">{username}</span>
                    </li>
                    <li className="user-detail__info-item">
                        <span className="user-detail__label">Email</span>
                        <span className="user-detail__text">{ email }</span>
                    </li>
                    <li className="user-detail__info-item">
                        <span className="user-detail__label">Phone</span>
                        <span className="user-detail__text"> {phone} </span>
                    </li>
                    <li className="user-detail__info-item">
                        <span className="user-detail__label">Website</span>
                        <span className="user-detail__text"> {website} </span>
                    </li>
                    <li className="user-detail__info-item">
                        <span className="user-detail__label">Company</span>
                        <span className="user-detail__text">{company && company.name}</span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default UserDetail;


