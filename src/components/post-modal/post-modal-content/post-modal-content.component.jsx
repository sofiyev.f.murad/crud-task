import React from "react";
import DialogContent from "@material-ui/core/DialogContent";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";

function PostModalContent({title, body, onChange: handleChange}) {
    return (
        <DialogContent dividers>

            <InputLabel htmlFor="title" className="post-modal__label">Email address</InputLabel>
            <TextField id="title" variant="outlined" fullWidth className="post-modal__input" value={title}
                       name="title"
                       onChange={handleChange}/>

            <InputLabel htmlFor="body" className="post-modal__label">Body</InputLabel>
            <TextField
                id="body"
                multiline
                rows={4}
                fullWidth
                variant="outlined"
                className="post-modal__input"
                value={body}
                name="body"
                onChange={handleChange}
            />
        </DialogContent>
    )
}

export default PostModalContent;