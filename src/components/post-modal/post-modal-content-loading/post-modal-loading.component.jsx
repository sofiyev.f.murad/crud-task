import React from "react";

import TextField from '@material-ui/core/TextField';
import Skeleton from "@material-ui/lab/Skeleton";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";
import DialogContent from "@material-ui/core/DialogContent";

function PostModalContentLoading() {
    return (
        <DialogContent dividers>
            <InputLabel htmlFor="email">Email address</InputLabel>
            <Typography variant="h1">
                <Skeleton />
            </Typography>

            <InputLabel htmlFor="body" >Body</InputLabel>
            <Typography variant="h1">
                <Skeleton />
            </Typography>
        </DialogContent>
    )
}

export default PostModalContentLoading;