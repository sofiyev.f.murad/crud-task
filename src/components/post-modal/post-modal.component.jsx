import React from "react";
import {makeStyles} from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Alert from "@material-ui/lab/Alert";


import Button from "../button/button.component";
import PostModalContent from "./post-modal-content/post-modal-content.component";
import PostModalContentLoading from "./post-modal-content-loading/post-modal-loading.component";


import "./post-modal.style.scss"


const useStyles = makeStyles({
    dialog: {
        position: 'absolute',
        left: "50%",
        transform: "translate(-50%, -50%)"
    }
});


function PostModal({className, loading, error, content, onClose: handleClose, onChange: handleContentChange, onSubmit: handleSubmit, ...otherProps}) {
    const classes = useStyles();

    return (
        <Dialog {...otherProps} fullWidth={true}
                classes={{paper: classes.dialog}}
        >
            <DialogTitle className="post-modal__title">
                Düzenle
                <IconButton aria-label="close" onClick={() => handleClose(false)} className="post-modal__close-button">
                    <CloseIcon/>
                </IconButton>
            </DialogTitle>

            {
                error ? (
                    <Alert severity="error">Bir hata oluştu</Alert>
                ) : null
            }


            {
                loading ? (
                    <PostModalContentLoading/>
                ) : (
                    <PostModalContent title={content && content.title}
                                      body={content && content.body}
                                      onChange={handleContentChange}/>
                )
            }
            <DialogActions className="post-modal__actions">
                <Button large color="primary" type="light-blue" disabled={loading} onClick={handleSubmit}>
                    GÜNCELLE
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default PostModal;