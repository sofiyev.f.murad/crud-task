import React from 'react'
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

const containerStyle = {
    width: '400px',
    height: '400px'
};

const options = {
    disableDefaultUI: true,
}

function UserMap({location}) {
    return (
        <LoadScript
            googleMapsApiKey="AIzaSyCDVxkivATNkkjWbXCrOejSQIfphWQgvjY"
        >
            <GoogleMap
                mapContainerStyle={containerStyle}
                center={location}
                zoom={5}
                options={options}

            >
                <Marker position={location} />
            </GoogleMap>
        </LoadScript>
    )
}

export default React.memo(UserMap)