import React from "react";
import {Alert} from "@material-ui/lab";

function PageContent( { loading, error, children, loadingComponent } ) {

    if(loading) {
        return loadingComponent
    }

    if (error) {
        return (
            <Alert severity="error">Hata oluştu <strong> <a href="">Sayfayı yeniden yükle </a></strong></Alert>
        )
    }

    return (
        <div>
            { children }
        </div>
    )
}

export default PageContent;