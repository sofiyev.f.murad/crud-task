import React from "react";

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Alert from '@material-ui/lab/Alert';

import Button from "../button/button.component";

function DeleteAlert({actionHandler, disableButton, error, ...otherProps}) {
    return (
        <Dialog
            {...otherProps}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">Bu postu silmek isdediğinize eminmisiniz?</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Bu postu silerseniz bir daha geri getirmek olmayacak. Bu postu silmek isdediğinize eminseniz evet
                    tuşuna basın
                </DialogContentText>

                {
                    error ? (
                        <Alert severity="error">Silme zamanı bir hata oluştu</Alert>
                    ) : null
                }

            </DialogContent>
            <DialogActions>
                <Button color="primary" onClick={() => actionHandler(true)} disabled={disableButton}>
                    Evet
                </Button>
                <Button color="primary" onClick={() => actionHandler(false)} autoFocus disabled={disableButton}>
                    Hayır
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DeleteAlert;