import React from "react";
import { Link } from "react-router-dom"

import TableCell from "@material-ui/core/TableCell";
import Button from "../../button/button.component";
import TableRow from "@material-ui/core/TableRow";

import "./post-table-row.style.scss"

function PostTableRow({ id, index, title, onDelete: handleDelete, onEdit: handleEdit, match }) {
    return (
        <TableRow className="post-table-row">
                <TableCell  className="post-table-row__table-cell" align="left">
                    <span className="post-table-row__index">{ index+1 } </span> { title }
                </TableCell>
                <TableCell  className="post-table-row__table-cell" align="right">
                    <Link  to={`post/${id}`}>
                        <Button className="post-table__button" type="purple" large>
                            DETAY
                        </Button>
                    </Link>
                    <Button className="post-table__button" type="green" large onClick={() => handleEdit(index, id)}>
                        DÜZENLE
                    </Button>
                    <Button className="post-table__button" type="orange" large onClick={() => handleDelete(index, id)}>
                        SİL
                    </Button>
                </TableCell>
        </TableRow>
    )
}

export default PostTableRow