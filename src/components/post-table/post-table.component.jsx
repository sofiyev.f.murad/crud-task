import React, {useEffect, useState} from "react";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import {Alert} from '@material-ui/lab';


import PostTableRow from "./post-table-row/post-table-row.component";


import "./post-table.style.scss";


function PostTable({ loading, error, posts, onDelete: handleDelete, onEdit: handleEdit }) {


    // if (loading) {
    //     return (
    //         <PostTableLoading/>
    //     )
    // }

    // if (error) {
    //     return (
    //         <Alert severity="error">Hata oluştu <strong> <a href="">Sayfayı yeniden yükle </a></strong></Alert>
    //     )
    // }

    if (!posts.length) {
        return (
            <Alert severity="info">Hiç bir sonuç bulunamadı</Alert>
        )
    }

    return (
        <TableContainer className="post-table">
            <Table>
                {
                    <TableBody>
                        {
                            posts.map(({id, title}, index) => (
                                <PostTableRow
                                    key={id} id={id} title={title} index={index}
                                    onDelete={handleDelete}
                                    onEdit={handleEdit}
                                />
                            ))
                        }
                    </TableBody>
                }
            </Table>
        </TableContainer>
    )
}

export default PostTable;