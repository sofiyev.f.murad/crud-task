import React from "react";

import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Skeleton from '@material-ui/lab/Skeleton';

import TableBody from "@material-ui/core/TableBody";


import "./post-table-loading.style.scss"
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";

function PostTableLoading({index, title}) {
    return (
        <TableContainer className="post-table">
            <Table>
                <TableBody>
                    {
                        new Array(4).fill(null).map((value, index) => {
                            return (
                                <TableRow key={index}>
                                    <TableCell className="post-table-loading__text-cell" align="left">
                                        <Skeleton className="post-table-loading__text-skeleton"/>
                                    </TableCell>
                                    <TableCell className="post-table-loading__button-cell" align="right">
                                        <Skeleton className="post-table-loading__button-skeleton"/>
                                    </TableCell>
                                </TableRow>
                            )
                        })
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default PostTableLoading;