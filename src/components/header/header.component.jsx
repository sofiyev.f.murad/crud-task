import React from "react";

import HeaderLogo from "./header-logo/header-logo.component";
import Nav from "../nav/nav.component";
import Button from "../button/button.component";

import "./header.style.scss";

function Header() {
    return (
        <div className="header">
            <HeaderLogo className="header__logo"/>
            <Nav className="header__nav" />
            <Button large color="primary">
                Login
            </Button>
            <div className="header__background" />
        </div>
    )
}

export default Header;