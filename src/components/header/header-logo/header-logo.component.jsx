import React from "react";
import classNames from "classnames"

import { ReactComponent as Logo } from "../../../assets/images/header-logo.svg"

import "./header-logo.style.scss";

function HeaderLogo({ className }) {
    const classes = classNames("header-logo", className, {

    })
    return (
        <div className={classes}>
            <Logo />
        </div>
    )
}

export default HeaderLogo;