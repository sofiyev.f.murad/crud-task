import React from "react";
import classNames from "classnames";

import MaterialButton from "@material-ui/core/Button";

import "./button.style.scss"

function Button({ children, className, type = "light", round, large, small, ...otherProps }) {

    const classes = classNames("button", className, type ? `button--${type}` : "", {
        "button--round": round,
        "button--large": large,
        "button--small": small
    })

    return (
            <MaterialButton className={classes} {...otherProps} >
                {children}
            </MaterialButton>
    )
}

export default Button;