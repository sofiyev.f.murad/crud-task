import React from "react";
import SvgIcon from '@material-ui/core/SvgIcon';

import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import TwitterIcon from '@material-ui/icons/Twitter';
import FacebookIcon from '@material-ui/icons/Facebook';
import GitHubIcon from '@material-ui/icons/GitHub';
import {ReactComponent as Dribble} from "../../assets/icon/dribble.svg";

import Nav from "../nav/nav.component";
import Button from "../button/button.component";

import "./footer.style.scss"

function Footer() {
    return (
        <footer className="footer">
            <div className="footer__top">
                <div className="footer__description">
                    <h2 className="footer__title">
                        Thank you for supporting us!
                    </h2>
                    <span className="footer__text">
                        Let's get in touch on any of these platforms.
                    </span>
                </div>

                <nav className="footer__social">
                    <ul className="footer__social-links">
                        <li className="footer__social-item">
                            <Tooltip title="Follow us" placement="top">
                                <span>
                                    <Button type="blue" small>
                                        <TwitterIcon className="footer__icon"/>
                                    </Button>
                                </span>
                            </Tooltip>
                        </li>
                        <li className="footer__social-item">
                            <Tooltip title="Follow us" placement="top">
                                <span>
                                    <Button type="dark-blue" small>
                                        <FacebookIcon className="footer__icon"/>
                                    </Button>
                                </span>
                            </Tooltip>
                        </li>
                        <li className="footer__social-item">
                            <Tooltip title="Follow us" placement="top">
                                <span>
                                   <Button type="pink" small>
                                        <SvgIcon className="footer__icon">
                                            <Dribble/>
                                        </SvgIcon>
                                    </Button>
                                </span>
                            </Tooltip>
                        </li>
                        <li className="footer__social-item">
                            <Tooltip title="Follow us" placement="top">
                                <span>
                                    <Button type="dark" small>
                                      <GitHubIcon className="footer__icon"/>
                                    </Button>
                                </span>
                            </Tooltip>
                        </li>
                    </ul>
                </nav>
            </div>

            <div className="footer__bottom">
                <span className="footer__copyright">
                    <span className="footer__year">
                        © 2018
                    </span>
                    <span className="footer__company">
                        Şikayetvar
                    </span>
                </span>
                <Nav className="footer__nav"/>
            </div>
        </footer>
    )
}

export default Footer