class HTTP {

    get = (url, query) => {
        if(query) {
            url = this._mergeUrlWithQuery(url, query);
        }
        return fetch(url).then(this._handleResponseJSON).catch(this._errorHandler)
    }

    delete = (url) => {
        return fetch(url, {
            method: 'DELETE',
        }).then(this._handleResponseJSON).catch(this._errorHandler)
    }

    put = (url, body) => {
        return fetch(url, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(this._handleResponseJSON).catch(this._errorHandler)
    }

    _errorHandler = (error) => {
        return {
            error: true,
            code: error.statusText,
            message: error.message,
        }
    }


    _mergeUrlWithQuery = (url, query) => {
        let result = url;

        if(typeof query === "object") {
            const queryString = this._jsonToQueryString(query);
            result = `${result}${queryString}`
        }

        if(typeof query === "string") {
            result = `${result}${query}`
        }

        return result;
    }

    _handleResponseJSON = (response) => {
        if(!response.ok) {
            return this._errorHandler(response);
        }
        return response.json()
    }

    _jsonToQueryString = (queryObject) => {
        return '?' +
            Object.keys(queryObject).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(queryObject[key]);
            }).join('&');
    }
}

export default HTTP;
