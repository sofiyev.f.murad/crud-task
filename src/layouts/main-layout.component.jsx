import React from "react";
import {createMuiTheme, ThemeProvider} from "@material-ui/core/styles";

import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";

import Header from "../components/header/header.component";
import Footer from "../components/footer/footer.component";

import "./main-layout.style.scss";

const theme = createMuiTheme({
    typography: {
        htmlFontSize: 10,
    },
});


function MainLayout({ children }) {
    return (
        <div className="main-layout">
            <ThemeProvider theme={theme}>
                <Container fixed>
                    <div className="main-layout__header">
                        <Header/>
                    </div>
                    <div className="main-layout__content">
                        <Paper className="main-layout__paper">
                            {children}
                        </Paper>
                    </div>
                    <div className="main-layout__footer">
                        <Footer />
                    </div>
                </Container>
            </ThemeProvider>
        </div>
    )
}

export default MainLayout;