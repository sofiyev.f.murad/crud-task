import React from "react";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";

import "./user-profile-loading.style.scss"

function UserProfileLoading() {
    return (
        <div className="user-profile-loading">
            <div className="user-profile-loading__header">
                <Typography variant="h1">
                    <Skeleton variant="text" />
                </Typography>
                <Typography variant="h6">
                    <Skeleton variant="text" />
                </Typography>
            </div>

            <div className="user-profile-loading__content">
                <Skeleton variant="rect" width="100%" height={118} />
            </div>
        </div>
    )
}

export default UserProfileLoading;