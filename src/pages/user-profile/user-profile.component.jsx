import React, {useEffect, useState} from "react";
import { withRouter } from "react-router-dom";
import ShikayetimVarSDK from "../../ShikayetimVarSDK";


import UserProfileLoading from "./user-profile-loading/user-profile-loading.component";
import UserDetail from "../../components/user-detail/user-detail.component";
import UserMap from "../../components/user-map/user-map.component";
import PageContent from "../../components/page-content/page-content.component";

import "./user-profile.style.scss";



const SDK = new ShikayetimVarSDK();
const users = SDK.users();

function UserProfile({ match }) {
    const { params: { id } = {}} = match;

    const [ userDetail, setUserDetail ] = useState({});
    const [ userDetailLoading, setUserDetailLoading ] = useState(true);
    const [ userDetailError, setUserDetailError ] = useState(null);

    useEffect(() => {
        users.get(id).then((response) => {
            if(response.error) {
                setUserDetailError(response);
            }
            setUserDetail(response);
        }).finally(() => {
            setUserDetailLoading(false);
        })
    }, [])


    const { address = {}, ...userData } = userDetail;

    const { geo = {} } = address;


    let { lat, lng } = geo;

    lat = +lat;
    lng = +lng;

    return (
        <PageContent
            loading={userDetailLoading}
            error={userDetailError}
            loadingComponent={<UserProfileLoading/>}
        >
            <div className="user-profile">
                <UserDetail userData={userData} address={address}/>
                <UserMap location={{ lat, lng }}/>
            </div>
        </PageContent>
    )
}

export default withRouter(UserProfile);