import React, {useEffect, useState} from "react";
import ShikayetimVarSDK from "../../ShikayetimVarSDK";

import PostTable from "../../components/post-table/post-table.component";
import DeleteAlert from "../../components/delete-alert/delete-modal.component";
import PostModal from "../../components/post-modal/post-modal.component";
import PostTableLoading from "../../components/post-table/post-table-loading/post-table-loading.component";
import PageContent from "../../components/page-content/page-content.component";

import "./posts.style.scss"



const SDK = new ShikayetimVarSDK();
const posts = SDK.posts();


function Posts() {

    const [postList, setPostList] = useState([]);
    const [postTableLoading, setPostTableLoading] = useState(true);
    const [postsLoadError, setPostsLoadError] = useState(null);

    const [postDeleteError, setPostDeleteError] = useState(null);
    const [deleteAlertStatus, setDeleteAlertStatus] = useState(false);
    const [deleteAlertButtonDisableStatus, setDeleteAlertButtonDisableStatus] = useState(false);

    const [postItem, setPostItem] = useState({});
    const [postFindError, setPostFindError] = useState(null);
    const [postModalLoading, setPostModalLoading] = useState(false);
    const [postModalStatus, setPostModalStatus] = useState(false);

    const [ postUpdateError, setPostUpdateError ] = useState(null)


    /* Start Post List Logic */

    useEffect(() => {
        posts.list().then((response) => {
            if (response.error) {
                setPostsLoadError(response);
                return
            }
            setPostList(response);
        }).finally(() => {
            setPostTableLoading(false);
        })
    }, [])

    /* End Post List Logic */


    /* Start Post Delete Logic  */

    useEffect(() => {
        if (!deleteAlertStatus) {
            Posts.deleteItemIndex = null;
            Posts.deleteItemId = null;
            setPostDeleteError(null);
        }
    }, [deleteAlertStatus])

    const handleDeletePost = (index, id) => {
        setDeleteAlertStatus(true);
        Posts.deleteItemIndex = index;
        Posts.deleteItemId = id;
    }

    const handleDeleteAlertAction = (action) => {
        if (action) {
            setDeleteAlertButtonDisableStatus(true);
            posts.delete(Posts.deleteItemId).then((response) => {
                if (response.error) {
                    setPostDeleteError(response);
                    return
                }
                let newPostList = postList.slice();
                newPostList.splice(Posts.deleteItemIndex, 1);
                setPostList(newPostList);
                setDeleteAlertStatus(false);
            }).finally(() => {
                setDeleteAlertButtonDisableStatus(false);
            })
        } else {
            setDeleteAlertStatus(false);
        }
    }

    /* End Post Delete Logic */



    /* Start Post Edit Logic */

    useEffect(() => {
        if (!postModalStatus) {
            Posts.editItemIndex = null;
            Posts.editItemId = null;
            setPostFindError(null);
            setPostItem(null);
            setPostModalLoading(false);
        }
    }, [postModalStatus])

    const handleEditPost = (index, id) => {
        setPostModalStatus(true);
        setPostModalLoading(true);
        Posts.editItemIndex = index;
        Posts.editItemId = id;
        posts.get(Posts.editItemId).then((response) => {
            if(response.error) {
                setPostFindError(response);
                return
            }
            setPostItem(response);
            setPostModalLoading(false);
        })
    }

    const handlePostModalChange = (e) => {
        const newPostItem = {...postItem};
        newPostItem[e.target.name] = e.target.value;
        setPostItem(newPostItem);
    }

    /* End Post Edit Logic */


    /* Start  Post Update Logic */

    const handlePostModalSubmit = () => {
        setPostModalLoading(true);
        posts.update(Posts.editItemId, postItem).then((response) => {
            if (response.error) {
                setPostUpdateError(response);
                return
            }
            const newPostList = postList.slice();
            newPostList.splice(Posts.editItemIndex, 1, response);
            setPostList(newPostList);
            setPostModalStatus(false);
        }).finally(() => {
            setPostModalLoading(false);
        })
    }

    /* End Post Update Logic */


    return (
        <div className="posts">
                <PageContent
                    loading={postTableLoading}
                    error={postsLoadError}
                    loadingComponent={<PostTableLoading/>}
                >
                    <PostTable
                        posts={postList}
                        onDelete={handleDeletePost}
                        onEdit={handleEditPost}
                    />
                </PageContent>

                <DeleteAlert open={deleteAlertStatus}
                             error={postDeleteError}
                             actionHandler={handleDeleteAlertAction}
                             disableButton={deleteAlertButtonDisableStatus}/>

                <PostModal open={postModalStatus} hideBackdrop={true} loading={postModalLoading}
                           content={postItem}
                           error={(postFindError || postUpdateError)}
                           onClose={setPostModalStatus}
                           onChange={handlePostModalChange}
                           onSubmit={handlePostModalSubmit}
                />
        </div>
    )
}

export default Posts