import React from "react";

import Skeleton from "@material-ui/lab/Skeleton";
import Typography from "@material-ui/core/Typography";

import "./post-detail-loading.style.scss"

function PostDetailLoading() {
    return (
        <div className="post-detail-loading">
            <div className="post-detail-loading__header">
                <Typography variant="h1">
                    <Skeleton variant="text" />
                </Typography>
                <Typography variant="h6">
                    <Skeleton variant="text" />
                </Typography>
            </div>

            <div className="post-detail-loading__content">
                <Skeleton variant="rect" width="100%" height={118} />
            </div>
        </div>
    )
}

export default PostDetailLoading;