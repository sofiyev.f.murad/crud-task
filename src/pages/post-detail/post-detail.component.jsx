import React, {useEffect, useState} from "react";
import { Link, withRouter } from "react-router-dom";
import ShikayetimVarSDK from "../../ShikayetimVarSDK";

import Typography from "@material-ui/core/Typography";


import PostDetailLoading from "./post-detail-loading/post-detail-loading.component";
import PageContent from "../../components/page-content/page-content.component";

import "./post-detail.style.scss"


const SDK = new ShikayetimVarSDK();

const posts = SDK.posts();

function PostDetail({ match }) {

    const { params: { id } = {}} = match;

    const [ postDetail, setPostDetail ] = useState({});
    const [ postDetailLoading, setPostDetailLoading ] = useState(true);
    const [ postDetailError, setPostDetailError ] = useState(null);

    useEffect(() => {
        posts.get(id).then((response) => {
            if(response.error) {
                setPostDetailError(response);
            }
            setPostDetail(response);
        }).finally(() => {
            setPostDetailLoading(false);
        })
    }, [])


    const { title = "", body = "", user = {} } = postDetail;
    const { id: userId, name: userName } = user;

    return (
        <PageContent
            loading={postDetailLoading}
            error={postDetailError}
            loadingComponent={<PostDetailLoading/>}
        >
            <div className="post-detail">
                <div className="post-detail__header">
                    <h2 className="post-detail__title"> { title } </h2>
                    <Link className="post-detail__user" to={`/profile/${userId}`}>{ userName }</Link>
                </div>
                <div className="post-detail__content">
                    <Typography gutterBottom>
                        {
                            body
                        }
                    </Typography>
                </div>
            </div>
        </PageContent>
    )
}

export default withRouter(PostDetail);