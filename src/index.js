import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from "react-router-dom";
import {StylesProvider} from '@material-ui/core/styles';

import App from './App';

import './index.scss';

ReactDOM.render(
    <React.StrictMode>
        <Router>
            <StylesProvider injectFirst>
                <App/>
            </StylesProvider>
        </Router>
    </React.StrictMode>,
    document.getElementById('root')
);
