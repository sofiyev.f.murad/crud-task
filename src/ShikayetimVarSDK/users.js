class Posts {
    constructor(http, baseUrl) {
        this.http = http;
        this.baseUrl = baseUrl;
    }

    get(id) {
        const url = `${this.baseUrl}users/${id}`
        return this.http.get(url)
    }
}

export default Posts;