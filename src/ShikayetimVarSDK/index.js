import HTTP from "../HTTP";

import Posts from "./posts";
import Users from "./users";


class ShikayetimVarSDK {

    constructor() {
        this.baseUrl = "https://jsonplaceholder.typicode.com/";
        this.http = new HTTP();
    }


    posts = () => {
        return new Posts(this.http, this.baseUrl)
    }

    users = () => {
        return new Users(this.http, this.baseUrl)
    }

}

export default ShikayetimVarSDK;