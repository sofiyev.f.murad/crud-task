class Posts {
    constructor(http, baseUrl) {
        this.http = http;
        this.baseUrl = baseUrl;
    }

    list({ start: _start = 0, limit:_limit = 10 } = {}) {
        const url = this.baseUrl + "posts";
        const pagination = {
            _start,
            _limit
        }
        return this.http.get(url, pagination)
    }

    get(id) {
        const url = `${this.baseUrl}posts/${id}?_expand=user`
        return this.http.get(url)
    }

    delete( id ) {
        const url = `${this.baseUrl}posts/${id}`
        return this.http.delete(url)
    }

    update( id, body ) {
        const url = `${this.baseUrl}posts/${id}`;
        return this.http.put(url, body);
    }
}

export default Posts;