import React from 'react';
import {Route, Switch} from "react-router-dom"


import MainLayout from "./layouts/main-layout.component";
import Posts from "./pages/posts/posts.component";
import PostDetail from "./pages/post-detail/post-detail.component";
import UserDetail from "./pages/user-profile/user-profile.component";

import './app.style.scss';



function App() {
    return (
        <div className="app">
            <Switch>
                <Route exact path="/">
                    <MainLayout>
                        <Posts/>
                    </MainLayout>
                </Route>

                <Route exact path="/post/:id">
                    <MainLayout>
                        <PostDetail/>
                    </MainLayout>
                </Route>

                <Route path="/profile/:id">
                    <MainLayout>
                        <UserDetail/>
                    </MainLayout>
                </Route>
            </Switch>
        </div>
    );
}

export default App;
